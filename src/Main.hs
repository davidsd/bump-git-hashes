{-# LANGUAGE DerivingStrategies         #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE LambdaCase                 #-}
{-# LANGUAGE OverloadedStrings          #-}

-- | Given a list of local packages that may depend on each other, we
-- would like to update their stack.yaml files so that the git hashes
-- of all dependencies are current -- i.e. they match HEAD for the
-- repository of that dependency.
--
-- To do this, we must walk through the packages in dependency order
-- (from least dependent to most dependent), and for each package set
-- the commit hashes of its dependencies in stack.yaml to HEAD. We
-- must also commit the new stack.yaml and this changes the commit
-- hash of the package we just edited. Thus, it is cruical to do this
-- sequentially. This is O(n^2) work that is best done by a script --
-- i.e. this one.

module Main where

import           Control.Applicative   ((<|>))
import           Control.Monad         (void, when)
import qualified Data.Aeson            as Aeson
import qualified Data.Graph            as Graph
import qualified Data.HashMap.Strict   as KeyMap
import           Data.List             (intercalate)
import           Data.Map              (Map)
import qualified Data.Map              as Map
import           Data.Set              (Set)
import qualified Data.Set              as Set
import           Data.Text             (Text)
import qualified Data.Text             as Text
import           Data.Traversable      (forM)
import           Data.Vector           (Vector)
import qualified Data.Vector           as Vector
import qualified Data.Yaml             as Yaml
import           System.Directory      (makeAbsolute, renameFile)
import           System.Environment    (getArgs)
import           System.FilePath.Posix (takeBaseName, (</>))
import           System.Process        (readCreateProcess, shell)

newtype Package = MkPackage FilePath
  deriving newtype (Eq, Ord, Show)

newtype StackYaml = MkStackYaml Aeson.Value
  deriving newtype (Eq, Ord, Show, Aeson.FromJSON, Aeson.ToJSON)

newtype PackageYaml = MkPackageYaml Aeson.Value
  deriving newtype (Eq, Ord, Show, Aeson.FromJSON, Aeson.ToJSON)

newtype PackageName = MkPackageName Text
  deriving newtype (Eq, Ord, Show)

stackYamlPath :: Package -> FilePath
stackYamlPath (MkPackage dir) = dir </> "stack.yaml"

packageYamlPath :: Package -> FilePath
packageYamlPath (MkPackage dir) = dir </> "package.yaml"

packageName :: Package -> PackageName
packageName (MkPackage dir) = MkPackageName $ Text.pack $ takeBaseName dir

-- | Read the stack.yaml file for the given package
getStackYaml :: Package -> IO (Either Yaml.ParseException StackYaml)
getStackYaml = Yaml.decodeFileEither . stackYamlPath

-- | Read the package.yaml file for the given package
getPackageYaml :: Package -> IO (Either Yaml.ParseException PackageYaml)
getPackageYaml = Yaml.decodeFileEither . packageYamlPath

-- | Run the given commands with the shell and return the output
readShell :: [String] -> IO String
readShell cmds =
  readCreateProcess (shell (intercalate "; " cmds)) ""

-- | Get the commit hash for HEAD for the given package
getCommitHash :: Package -> IO Text
getCommitHash (MkPackage dir) =
  fmap (Text.pack . init) $ readShell
  [ "cd " <> dir
  , "git rev-parse HEAD"
  ]

-- | Write the given 'StackYaml' to a stack.yaml file for the given
-- package.
writeStackYaml :: Package -> StackYaml -> IO ()
writeStackYaml package yaml = do
  let
    yamlPath = stackYamlPath package
    yamlPathTmp = yamlPath <> "_tmp"
  Yaml.encodeFile yamlPathTmp yaml
  putStrLn $ "Writing stack.yaml: " <> yamlPath
  renameFile yamlPathTmp yamlPath

-- | Given the contents of a package.yaml file, find the list of
-- dependencies listed at the top level or under library.
parseDependencies :: PackageYaml -> Set PackageName
parseDependencies (MkPackageYaml (Aeson.Object o)) =
  case topLevel o <|> libraryLevel of
    Just deps -> Set.fromList (Vector.toList deps)
    Nothing   -> error $ "Couldn't find dependencies" <> show o
  where
    topLevel o' = do
      Aeson.Array deps <- KeyMap.lookup "dependencies" o'
      forM deps $ \case
        Aeson.String d -> pure (MkPackageName d)
        _              -> Nothing
    libraryLevel = do
      Aeson.Object lib <- KeyMap.lookup "library" o
      topLevel lib

-- | Given the Yaml data for a stack.yaml file, update the commit hash
-- for the given package in extra-deps, if it exists.
setCommitHash :: PackageName -> Text -> StackYaml -> StackYaml
setCommitHash (MkPackageName name) hash (MkStackYaml (Aeson.Object o)) = MkStackYaml $ Aeson.Object $
  KeyMap.adjust (\(Aeson.Array deps) -> Aeson.Array (fmap updateDep deps)) "extra-deps" o
  where
    updateDep dep@(Aeson.Object o') =
      case KeyMap.toList o' of
        [ g@("git", Aeson.String url), ("commit", Aeson.String _) ]
          | Text.pack (takeBaseName (Text.unpack url)) == name ->
              Aeson.Object $ KeyMap.fromList [g, ("commit", Aeson.String hash)]
        _ -> dep
    updateDep dep = dep
setCommitHash _ _ y = y

-- | Given a list of packages, determine their mutual dependencies, in
-- suitable form for 'Graph.graphFromEdges'.
getMutualDependencies :: [Package] -> IO [(Package, PackageName, [PackageName])]
getMutualDependencies packages = forM packages $ \p -> do
  deps <- getPackageYaml p >>= \case
    Left _ -> do
      putStrLn $ "Couldn't read package.yaml for " <> show (packageName p)
      pure Set.empty
    Right y -> pure $ parseDependencies y
  pure $
    ( p
    , packageName p
    , Set.toList (Set.intersection deps packageNames)
    )
  where
    packageNames = Set.fromList (map packageName packages)

-- | Sort the packages in dependency order, where the first element
-- has no dependnecies (among the given set of packages), and the last
-- element has the most dependencies.
depSortPackages :: [Package] -> IO [(Package, PackageName)]
depSortPackages packages = do
  m <- getMutualDependencies packages
  let
    (g, fromVertex) = Graph.graphFromEdges' m
  pure $ reverse $
    (\(a,b,_) -> (a,b)) . fromVertex <$> Graph.topSort g

-- | Given a package and its list of dependencies, update the
-- stack.yaml file for the package so that the commit hashes of its
-- dependencies are current (i.e. match HEAD). Furthermore, check the
-- stack.yaml file into the repository and commit. Note that this
-- changes the commit hash for the given package.
updatePackageStackYaml :: Package -> [(Package, PackageName)] -> IO ()
updatePackageStackYaml package@(MkPackage dir) deps = do
  let name = show (packageName package)
  getStackYaml package >>= \case
    Left _ -> putStrLn $ "Couldn't read stack.yaml for " <> name
    Right stackYaml -> do
      putStrLn $ "Updating package: " <> name
      setHashes <- forM deps $ \(dep, depName) -> do
        hash <- getCommitHash dep
        pure (setCommitHash depName hash)
      let stackYaml' = foldr (.) id setHashes stackYaml
      when (stackYaml' /= stackYaml) $ do
        writeStackYaml package stackYaml'
        let cmds =
              [ "cd " <> dir
              , "git add " <> stackYamlPath package
              , "git commit -m \"bump dependencies\""
              ]
        mapM (\c -> putStrLn ("> " <> c)) cmds
        void $ readShell cmds

-- | Sort packages in dependency order, and one by one update their
-- stack.yaml files so that their dependencies have current
-- hashes. Since 'updatePackageStackYaml' changes the commit hash of a
-- package, it is crucial to do this sequentially.
updatePackagesStackYaml :: [Package] -> IO ()
updatePackagesStackYaml unsortedPackages = do
  packages <- depSortPackages unsortedPackages
  go packages []
  where
    go [] _ = pure ()
    go (p@(package,_) : ps) deps = do
      updatePackageStackYaml package deps
      go ps (p : deps)

main :: IO ()
main = do
  paths <- getArgs
  packages <- traverse (fmap MkPackage . makeAbsolute) paths
  putStrLn $ "Found packages: " <> show (packageName <$> packages)
  updatePackagesStackYaml packages
