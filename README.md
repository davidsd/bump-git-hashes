# bump-git-hashes

Given a list of local packages that may depend on each other, we would
like to update their `stack.yaml` files so that the git hashes of all
dependencies are current (i.e. equal to `HEAD`).

To do this, we must walk through the packages in dependency order
(from packages with no dependencies to packages 
with the most dependencies). For each package, we must set the
commit hashes of its dependencies in `stack.yaml` to `HEAD`. We must also
commit the new `stack.yaml`. This changes the commit hash of the
package we just edited. Thus, it is cruical to do this
sequentially. This is O(n^2) work that is best done by a script --
i.e. this one.

# Installation

    stack install

# Usage

  1. Make sure that all packages to be updated have **changes
  already committed**.
  This script will call 'git commit', so it could really
  mess things up if you have changes (either staged or unstaged). If you run `git status` in each package, it should look like:

          # On branch master
          nothing to commit, working directory clean

  2. Run:

          bump-git-hashes path/to/package1 path/to/package2 ... path/to/packageN


